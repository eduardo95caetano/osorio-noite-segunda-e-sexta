import java.util.Scanner;

public class Aula3 {
    
    public static void main(String[] args) {
        
        Scanner leitor = new Scanner(System.in);

        int qtdNumeros = 5;
        int[] numeros = new int[5];


        System.out.println("~ Digite cinco numeros ~ ");
        
        for(int i = 0; i < qtdNumeros; i++){
            System.out.print("Numero (" + (i+1) + "): ");
            numeros[i] = Integer.parseInt(leitor.nextLine());
        }

        int soma = 0;

        System.out.print("Números no array: ");
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i]);
            System.out.print(" - ");
            soma += numeros[i];
        }
        
        float media = soma / numeros.length;
        System.out.print("Média: " + media);


    }

}