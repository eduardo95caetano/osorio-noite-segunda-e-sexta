/*
    int[] arrayOfInts; // preferred
    int [] arrayOfInts; // spacing is optional
    int arrayOfInts[]; // C-style, allowed


    JLabel[] threeLabels = new JLabel[3];
    JLabel[] threeLabels = { null, null, null };
    

    int number = 10;
    int[] arrayOfInts = new int[42];
    


    String names[] = new String[42];
    // names[0] == null;
    // names[1] == null;

    int[] primes = { 2, 3, 5, 7, 7+4 };
    print(primes) ==> int[5] { 2, 3, 5, 7, 11 }
    print(primes[2]) ==> 5
    print(primes[4]) ==> 11

    .length



    jshell> byte[] bar = new byte[] { 1, 2, 3, 4, 5 }
    bar ==> byte[5] { 1, 2, 3, 4, 5 }

    jshell> byte[] barCopy = Arrays.copyOf(bar, bar.length)
    barCopy ==> byte[5] { 1, 2, 3, 4, 5 }

    jshell> byte[] expanded = Arrays.copyOf(bar, bar.length+2)
    expanded ==> byte[7] { 1, 2, 3, 4, 5, 0, 0 }

    jshell> byte[] firstThree = Arrays.copyOfRange(bar, 0, 3)
    firstThree ==> byte[3] { 1, 2, 3 }

    jshell> byte[] lastThree = Arrays.copyOfRange(bar, 2, bar.length)
    lastThree ==> byte[3] { 3, 4, 5 }

    jshell> byte[] plusTwo = Arrays.copyOfRange(bar, 2, bar.length+2)
    plusTwo ==> byte[5] { 3, 4, 5, 0, 0 }


    */