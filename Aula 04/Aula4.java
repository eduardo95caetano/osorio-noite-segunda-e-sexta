import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Aula4 {
    
    public static void main(String[] args) {
        
        Object[] array = new Object[5];
        array[0] = 10;
        array[1] = "String";
        array[2] = true;
        array[3] = 32.1f;

        System.out.println(array[2]);
        System.out.println(Arrays.toString(array));
        // [10, "String", true, 32.1, null]


        List<Integer> lista = new ArrayList<Integer>();
        System.out.println(lista.toString()); // []
        
        lista.add(30);
        lista.add(450);
        lista.add(22);
        System.out.println(lista.toString()); // [30, 450, 22]
        
        lista.remove(1);
        System.out.println(lista.toString()); // [30, 22]
        
        System.out.println(lista.get(1)); 


    }

}
