import java.util.Arrays;
import java.util.Scanner;

public class Ex15 {
    
    static float lerNumero(String texto){
        Scanner leitor = new Scanner(System.in);

        System.out.print(texto);
        float num = Float.parseFloat(leitor.nextLine());
        
        return num;
    }

    
    public static void main(String[] args) {
        
        float[] numeros = new float[5];
        
        System.out.println(" ~ digite cinco numeros ~");
        
        numeros[0] = lerNumero("Digite o numero (1): ");
        

        for (int i = 1; i < numeros.length; i++) {

            float num;
            do {
                num = lerNumero("Digite o numero (" + (i+1) + ")");
            }while(num <= numeros[i-1]);
            
            numeros[i] = num;
            
        }

        System.out.println(Arrays.toString(numeros));
    }

}
