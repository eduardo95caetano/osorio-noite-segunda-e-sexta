import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Ex02 {

    static int lerNumero(String texto){
        Scanner leitor = new Scanner(System.in);

        System.out.print(texto);
        int num = Integer.parseInt(leitor.nextLine());
        
        return num;
    }

    static int somar(int a, int b, boolean mostrar){

        int soma = a + b;

        if(mostrar){
            System.out.println("A soma de " + a + " + " + b + " é " + soma);
        }

        return soma;
    }

    static int somar(int[] numeros, boolean mostrar){
        int soma = 0;
        for (int i = 0; i < numeros.length; i++) {
            soma += numeros[i];
        }

        System.out.println("A soma do array " + Arrays.toString(numeros) + " é " + soma);
        return soma;
    }

    public static void main(String[] args) {
        
        int[] numeros = new int[5];

        System.out.println(" ~ digite dois numeros ~");

        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = lerNumero("Digite o numero " + (i+1) + ": ");
        }

        somar(numeros, true);

        // int num1 = lerNumero("Primeiro numero: ");
        // int num2 = lerNumero("Segundo numero: ");
        
        // int soma = somar(num1, num2, true);

    }

}