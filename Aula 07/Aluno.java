package Aula6;
import java.util.ArrayList;
import java.util.List;

public class Aluno {
    
    
    String matricula;
    String nome;
    // float[] notas = new float[5];
    List<Float> notas = new ArrayList<>();

    float nota1;
    float nota2;
    float media;

    boolean aprovado;

    Aluno(){
        System.out.println("~ criando novo aluno vazio ~");
    }

    Aluno(String mat, String nom){
        System.out.println("~ criando novo aluno (simples) - " + nom + " ~");
        matricula = mat;
        nome = nom;
    }


    Aluno(String mat, String nom, float n1, float n2){
        System.out.println("~ criando novo aluno (completo) - " + nom + " ~");
        matricula = mat;
        nome = nom;
        nota1 = n1;
        nota2 = n2;
    }

    void calcularMedia(){
        
        float somaNota = 0;
        
        for(int i = 0; i < notas.size(); i++){
            somaNota += notas.get(i);
        }
        
        media = somaNota / notas.size();

        /*
            if(mostrar){
                System.out.println("Média do Aluno " + nome);
                System.out.println("-- Nota 1: " + nota1);
                System.out.println("-- Nota 2: " + nota2);
                System.out.println("-- Média Final: " + media);
            }
        */
    }


    void mostrar(){
        System.out.println("\nMostrando Aluno: " + nome);
        System.out.println("-- Numero Matricula: " + matricula);
        System.out.println("-- Notas: ");
    
        for(int i = 0; i < notas.size(); i++){
            System.out.println("---- Nota (" + (i+1) + "): " + notas.get(i));
        }

        calcularMedia();
        System.out.println("-- Media: " + media);

        if(media >= 6.0f)
            System.out.println("-- Status: (X) Aprovado ( ) Reprovado");
        else
            System.out.println("-- Status: ( ) Aprovado (X) Reprovado");

    }

}
