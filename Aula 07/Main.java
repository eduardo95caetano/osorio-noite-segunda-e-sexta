package Aula6;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       

        // Aluno aluno1 = new Aluno("AL01", "José", 9.7f, 3.2f);
        // Aluno aluno2 = new Aluno("AL02", "Ricardo", 3.2f, 7.89f);

        Scanner leitor = new Scanner(System.in);
        List<Aluno> turma = new ArrayList<Aluno>();

        System.out.print("Quantos alunos quer adicionar? ");
        int quantidade = Integer.parseInt(leitor.nextLine());
        
        for(int i = 0; i < quantidade; i++){
            // System.out.print("~ Ciando novo Aluno");
            Aluno novoAluno = new Aluno();

            System.out.print("-- Digite a matricula: ");
            novoAluno.matricula = leitor.nextLine();
            
            System.out.print("-- Digite o nome: ");
            novoAluno.nome = leitor.nextLine();
            
            System.out.print("Quantas notas quer adicionar? ");
            int qtd_notas = Integer.parseInt(leitor.nextLine());

            for(int j = 0; j < qtd_notas; j++){
                System.out.print("--- Digite a nota (" + (j+1) + "):");
                float notaDigitada = Float.parseFloat(leitor.nextLine());
                novoAluno.notas.add(notaDigitada);
            }

            turma.add(novoAluno);

        }
        
        for(int i = 0; i < turma.size(); i++){
            turma.get(i).mostrar();
        }

        

    }
}
