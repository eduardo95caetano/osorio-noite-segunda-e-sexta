import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        
        Televisao tv = new Televisao(false, 52, 90);

        tv.LigarTv();
        tv.LigarTv();
        tv.LigarTv();
        tv.LigarTv();
        
        // tv.setLigado(true);
        tv.setCanal(40);
        
        tv.aumentarVolume();
        tv.aumentarVolume();
        tv.aumentarVolume();
        tv.aumentarVolume();
        tv.aumentarVolume();
        tv.aumentarVolume();

        System.out.println("\n--------");
        System.out.println("Canal Tv: " + tv.getCanal());
        System.out.println("Volume Tv: " + tv.getVolume());
        System.out.println("Tv Ligada: " + tv.getLigado());


    }

}
