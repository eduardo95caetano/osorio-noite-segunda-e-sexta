public class Televisao {
    
    private int canal;
    private int volume;
    private boolean ligado;


    Televisao(){
        this.canal = 3;
        this.volume = 50;
        this.ligado = true;
    }

    Televisao(boolean ligado, int volume, int canal){
        this.canal = canal;
        this.volume = volume;
        this.ligado = ligado;
    }   


    void LigarTv(){
        System.out.println(">>> Botão [LIGA/DESLIGA]");

        if(this.getLigado()){
            this.setLigado(false);
        }else{
            this.setLigado(true);
        }
    }

    void aumentarVolume(){
        System.out.println(">>> Botão [VOLUME +]");
        // volume += 5;

        if(this.getVolume() <= 95){
            this.setVolume(this.getVolume() + 5);
        }else{
            System.out.println(">> Volume no máximo >.<");
        }
    }


    void setVolume(int volume){
        this.volume = volume;
        System.out.println(">> Volume alterado para: " + volume);
    }

    int getVolume(){
        return this.volume;
    }

    void setCanal(int canal){
        this.canal = canal;
        System.out.println(">> Canal alterado para: " + canal);
    }

    int getCanal(){
        return this.canal;
    }

    void setLigado(boolean ligado){
        this.ligado = ligado;
        System.out.println(">> Ligado alterado para: " + this.ligado);
    }

    boolean getLigado(){
        return this.ligado;
    }

}
