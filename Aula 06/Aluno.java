public class Aluno {
    
    
    public String matricula;
    public String nome;

    private float nota1;
    private float nota2;
    private float media;

    public boolean aprovado;

    public void setNota1(float novaNota){
        nota1 = novaNota; // + criterios
    }

    public float getNota1(){
        return nota1;
    }


    Aluno(){
        System.out.println("~ criando novo aluno vazio ~");
    }

    Aluno(String mat, String nom){
        System.out.println("~ criando novo aluno (simples) - " + nom + " ~");
        matricula = mat;
        nome = nom;
    }


    Aluno(String mat, String nom, float n1, float n2){
        System.out.println("~ criando novo aluno (completo) - " + nom + " ~");
        matricula = mat;
        nome = nom;
        nota1 = n1;
        nota2 = n2;
    }

    void calcularMedia(){
        media = (nota1 + nota2) / 2;
        System.out.println("Média do Aluno " + nome);
        System.out.println("-- Nota 1: " + nota1);
        System.out.println("-- Nota 2: " + nota2);
        System.out.println("-- Média Final: " + media);
    }

}
