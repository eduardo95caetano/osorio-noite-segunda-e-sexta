import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Criando um objeto do tipo aluno
        /*Aluno aluno1 = new Aluno();
        Aluno aluno2 = new Aluno();
        
        // Setando valores do obj aluno1
        aluno1.matricula = "AL01";
        aluno1.nome = "José";
        aluno1.nota1 = 9.7f;
        aluno1.nota2 = 3.2f;
        
        aluno2.matricula = "AL02";
        aluno2.nome = "Ricardo";
        aluno2.nota1 = 3.2f;
        aluno2.nota2 = 7.89f;

        aluno1.calcularMedia();
        aluno2.calcularMedia();*/

        List<Aluno> turma = new ArrayList<Aluno>();

        Aluno aluno1 = new Aluno("AL01", "José", 9.7f, 3.2f);
        Aluno aluno2 = new Aluno("AL02", "Ricardo", 3.2f, 7.89f);

        aluno1.calcularMedia();
        aluno2.calcularMedia();

        turma.add(aluno1);
        turma.add(aluno2);
        
        turma.get(0);
        turma.get(1);

    }
}
